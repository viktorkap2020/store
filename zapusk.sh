git pull

# поиск и установка необходимых пакетов для работы с проектом.
composer install

# Запуск сервера
symfony serve

# Накатываем миграции
echo "yes" | php bin/console doctrine:migrations:migrate

# Если надо сгенерить миграцию:
php bin/console make:migration

# Если надо пересоздать БД
php bin/console doctrine:schema:drop --force
echo "yes" | php bin/console doctrine:migrations:version --delete --all
echo "yes" | php bin/console doctrine:migrations:migrate
php bin/console insert-test-data

# Для генерации CRUD контроллеров
php bin/console make:admin:crud

# Вход Админ '/login' :
# login: admin
# password: admin


