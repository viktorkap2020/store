<?php

namespace App\Command;

use App\Entity\Admin;
use App\Entity\Category;
use App\Entity\Products;
use App\Repository\ProductsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InsertTestDataCommand extends Command
{
    protected static $defaultName = 'insert-test-data';

    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(string $name = null, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $category = new Category();
        $category->setName('Смартфоны и планшеты');
        $this->entityManager->persist($category);

        $product = new Products();
        $product->setName('Смартфон')->setPrice(4500)->setCategory($category);
        $this->entityManager->persist($product);

        $product = new Products();
        $product->setName('Планшет')->setPrice(12400)->setCategory($category);
        $this->entityManager->persist($product);

        $category = new Category();
        $category->setName('Ноутбуки');
        $this->entityManager->persist($category);

        $product = new Products();
        $product->setName('Ноутбук')->setPrice(30700)->setCategory($category);
        $this->entityManager->persist($product);

        $password = 'admin';
        $admin = new Admin();
        $admin
            ->setUsername('admin')
            ->setRoles(["ROLE_ADMIN"]);
        $encodedPassword = $this->passwordEncoder->encodePassword($admin, $password);
        $admin->setPassword($encodedPassword);
        $this->entityManager->persist($admin);

        $this->entityManager->flush();

        $io->text('Тестовые данные успешно добавлены');

        return Command::SUCCESS;
    }
}
