<?php

namespace App\Controller;

use App\Entity\ProductInOrder;
use App\Entity\Orders;
use App\Entity\Products;
use App\Form\OrdersFormType;
use App\Repository\CategoryRepository;
use App\Repository\ProductsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrdersRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class CartController extends AbstractController
{
    private array $cart;

    public function __construct(SessionInterface $session, EntityManagerInterface $entityManager)
    {
        $this->session = $session;
        /**
         * $cart  - это массив вида
         * [
         *      id_продукта 1 => количество в корзине 1,
         *      id_продукта 2 => количество в корзине 2,
         *      и тд
         * ]
         */
        $this->cart = $this->session->get('cart', []);
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/add_to_cart/{id}", name="add_to_cart")
     * @param int $id
     * @param Session $session
     * @param Request $request
     * @return Response
     */
    public function addToCart(int $id, Session $session, Request $request): Response
    {
        // Если в корзине уже был этот товар - увеличиваем кол-во на единицу
        if (isset($this->cart[$id])) {
            $this->cart[$id]++;
        } else {
            // Если товара раньше не было - ставим количество = 1
            $this->cart[$id] = 1;
        }

        $session->set('cart', $this->cart);
        return $this->redirect($request->headers->get('referer', '/'));
    }

    private function getIdToProduct(ProductsRepository $productsRepository)
    {
        $products = !empty($this->cart)
            ? $productsRepository->findBy(['id' => array_keys($this->cart)])
            : [];

        $idToProduct = [];
        foreach ($products as $product) {
            $idToProduct[$product->getId()] = $product;
        }

        return $idToProduct;
    }

    private function getProductSum(ProductsRepository $productsRepository)
    {
        $idToProduct = $this->getIdToProduct($productsRepository);
        $orderSum = 0;
        foreach ($idToProduct as $id => $product) {
            $orderSum += $product->getPrice() * $this->cart[$id];
        }
        return $orderSum;
    }

    /**
     * @Route("/cart", name="cart")
     * @param Session $session
     * @param ProductsRepository $productsRepository
     * @param Request $request
     * @return Response
     */
    public function cart(Session $session, ProductsRepository $productsRepository, Request $request): Response
    {
        $this->updateCartCountsFromForm($session, $request);
        $idToProduct = $this->getIdToProduct($productsRepository);

        return $this->render('main/cart.html.twig', [
            'cart'          => $this->cart,
            'id_to_product' => $idToProduct,
            'order_sum'     => $this->getProductSum($productsRepository),
        ]);
    }

    /**
     * @param Session $session
     * @param Request $request
     *
     * На странице /cart заполняется форма "продукт - количество". Из неё идёт POST-запрос на страницу /cart, где для продукта с id=1,2,... передаются в запросе данные count_1, count_2, ...
     *
     * Задача метода - запомнить эти данные в сессии.
     */
    private function updateCartCountsFromForm(Session $session, Request $request)
    {
        foreach ($this->cart as $id => $value) {
            // Берём кол-во продукта из инпутов на странице /cart
            $count = $request->request->get("count_{$id}");

            if (!is_numeric($count)) {
                // Если вдруг инпут не заполнен, то оставляем то количество, которое уже было в корзине
                $count = $this->cart[$id];
            }

            $this->cart[$id] = $count;
        }
        $session->set('cart', $this->cart);
    }

    /**
     * @Route("/preview_order", name="preview_order")
     */

    public function previewOrder(ProductsRepository $productsRepository): Response
    {
        $products = $this->getIdToProduct($productsRepository);
        foreach ($products as $product)
        {
            $productName = $product->getName();
        }

        foreach ($this->cart as $id => $count)
        {
            $productCount = $count;
        }
        return $this->render('main/preview_order.html.twig', [
            'cart' => $this->cart,
            'product_name' => $productName,
            'product_count' => $productCount,
            'order_sum' => $this->getProductSum($productsRepository),
        ]);
    }

    /**
     * @Route("/checkout", name="checkout")
     */

    public function checkout(): Response
    {
        $orders = new Orders();
        $form = $this->createForm(OrdersFormType::class, $orders);

        return $this->render('main/checkout.html.twig', [
            'cart'        => $this->cart,
            'orders_form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/create_order", name="create_order")
     * @param EntityManagerInterface $entityManager
     * @param ProductsRepository $productsRepository
     * @param Request $request
     * @return Response
     */
    public
    function createOrder(
        EntityManagerInterface $entityManager,
        ProductsRepository $productsRepository,
        Request $request
    ): Response
    {
        $orders = new Orders();
        $form = $this->createForm(OrdersFormType::class, $orders);
        $form->handleRequest($request);

        $idToProduct = $this->getIdToProduct($productsRepository);
        foreach ($idToProduct as $id => $product) {
            $productInOrder = new ProductInOrder();
            $productInOrder->setProductCount($this->cart[$id]);
            $productInOrder->setProduct($product);
            $entityManager->persist($productInOrder);

            $orders->addProductInOrder($productInOrder);
        }
        $orders->setOrderSum($this->getProductSum($productsRepository));

        $entityManager->persist($orders);
        $entityManager->flush();
        return $this->render('main/create_order.html.twig', []);
    }

    /**
     * @Route("view_order", name="view_order")
     */
    public function viewOrder(OrdersRepository $ordersRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        $orders = $ordersRepository->viewOrdersUsingQueryBuilder();

        return $this->render('main/view_order.html.twig', [
            'orders' => $orders,
        ]);
    }
}
