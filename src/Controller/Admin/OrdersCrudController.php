<?php

namespace App\Controller\Admin;

use App\Entity\Orders;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrdersCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Orders::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('firstName')->setSortable(true),
            TextField::new('lastName')->setSortable(true),
            TextField::new('email')->setSortable(true),
            TextField::new('city')->setSortable(true),
            TextField::new('telephoneNumber')->setSortable(true),
            MoneyField::new('orderSum')->setSortable(true)->setCurrency('UAH')->setCustomOption(MoneyField::OPTION_STORED_AS_CENTS, false),
        ];
    }
}
