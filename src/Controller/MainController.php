<?php

namespace App\Controller;

use App\Entity\ProductInOrder;
use App\Entity\Orders;
use App\Entity\Products;
use App\Repository\CategoryRepository;
use App\Repository\ProductsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrdersRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('main/index.html.twig', [
            'category' => $categoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/category/{id}", name="category")
     */
    public function category(
        int $id,
        CategoryRepository $categoryRepository): Response
    {
        $category = $categoryRepository->find($id);
        if (empty($category)) {
            die("Категории {$id} не существует! Но вы можете перейти на <a href='/'>главную страницу</a> и выбрать другие категории");
        }

        return $this->render('main/products.html.twig', [
            'products' => $category->getProducts(),
        ]);
    }
}
