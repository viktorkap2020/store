<?php

namespace App\Repository;

use App\Entity\Orders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Orders|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orders|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orders[]    findAll()
 * @method Orders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Orders::class);
    }

    public function viewOrdersUsingQueryBuilder()
    {
        return $this->createQueryBuilder('o')
            ->addSelect('pio')->join('o.productInOrders', 'pio')
            ->addSelect('p')->join('pio.product', 'p')
            ->addSelect('c')->join('p.category', 'c')
            ->getQuery()
            ->getResult();
    }
}
