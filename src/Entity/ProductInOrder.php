<?php

namespace App\Entity;

use App\Repository\ProductInOrderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductInOrderRepository::class)
 */
class ProductInOrder
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_count;

    /**
     * @ORM\ManyToOne(targetEntity=Products::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=Orders::class, inversedBy="productInOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currentOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductCount(): ?int
    {
        return $this->product_count;
    }

    public function setProductCount(int $product_count): self
    {
        $this->product_count = $product_count;

        return $this;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCurrentOrder(): ?Orders
    {
        return $this->currentOrder;
    }

    public function setCurrentOrder(?Orders $currentOrder): self
    {
        $this->currentOrder = $currentOrder;

        return $this;
    }

    public function __toString(): string
    {
        return "{$this->getProduct()->getName()}: {$this->getProductCount()} шт";
    }
}
