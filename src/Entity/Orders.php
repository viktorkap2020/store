<?php

namespace App\Entity;

use App\Repository\OrdersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OrdersRepository::class)
 */
class Orders
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Email
     *
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $telephoneNumber;

    /**
     * @ORM\Column(type="integer")
     */
    private $orderSum;

    /**
     * @ORM\OneToMany(targetEntity=ProductInOrder::class, mappedBy="currentOrder", orphanRemoval=true)
     */
    private $productInOrders;

    public function __construct()
    {
        $this->productInOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getTelephoneNumber(): ?string
    {
        return $this->telephoneNumber;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function setTelephoneNumber(string $telephoneNumber): self
    {
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }

    public function getOrderSum(): ?int
    {
        return $this->orderSum;
    }

    public function setOrderSum(int $orderSum): self
    {
        $this->orderSum = $orderSum;

        return $this;
    }

    /**
     * @return Collection|ProductInOrder[]
     */
    public function getProductInOrders(): Collection
    {
        return $this->productInOrders;
    }

    public function addProductInOrder(ProductInOrder $productInOrder): self
    {
        if (!$this->productInOrders->contains($productInOrder)) {
            $this->productInOrders[] = $productInOrder;
            $productInOrder->setCurrentOrder($this);
        }

        return $this;
    }

    public function removeProductInOrder(ProductInOrder $productInOrder): self
    {
        if ($this->productInOrders->removeElement($productInOrder)) {
            // set the owning side to null (unless already changed)
            if ($productInOrder->getCurrentOrder() === $this) {
                $productInOrder->setCurrentOrder(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return "{$this->getFirstName()} {$this->getLastName()}, {$this->getCity()} (id = {$this->getId()})";
    }
}
